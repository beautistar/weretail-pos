//
//  ProductModel.swift
//  WeRetail POS
//
//  Created by Developer on 10/29/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProductModel {
    
    var product_id = ""
    var product_name = ""
    var category = ""
    var short_description = ""
    var sub_category = ""
    var date = ""
    var thumbnail_url = ""
    var page_url = ""
    var price = ""
    
    init?(json: JSON?) {
        guard let json = json else { return nil }
        product_id = json["productid"].stringValue
        product_name = json["productname"].stringValue
        category = json["category"].stringValue
        short_description = json["shortdescription"].stringValue
        sub_category = json["subcategory"].stringValue
        date = json["date"].stringValue
        thumbnail_url = json["thumbnailUrl"].stringValue
        page_url = json["product_id"].stringValue
        price = json["price"].stringValue
        
    }
}
