//
//  MainViewController.swift
//  WeRetail POS
//
//  Created by Developer on 10/26/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblProductList: UITableView!
    @IBOutlet weak var tblPurchasedList: UITableView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblSubTotal: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTax: UILabel!
    @IBOutlet weak var lblGrantTotal: UILabel!
    
    
    var server_url  = "http://repackagedweb.com/weretail/msg/product2.txt"

    
    var categories = [CategoryModel]()
    
    var purchasedProducts = [PurchsedProductModel]()

    var storedOffsets = [Int: CGFloat]()

    
    var productItemCellWidth: CGFloat = 80
    var productItemCellHeight: CGFloat = 100
    var discount: Float = 4.99
    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblProductList.tableFooterView = UIView()
        tblPurchasedList.tableFooterView = UIView()
        
        getProducts()

    }
    
    
    func getProducts() {
        
        Alamofire.request(server_url, method: .get, parameters: nil).responseJSON { (response) in
            
            print(response)
            
            if response.result.isFailure{
                //completion(Const.ERROR_CONNECT, [])
            }
            else
            {
                let json = JSON(response.result.value!)
                
                self.categories = json.arrayValue.compactMap { CategoryModel(json: $0) }
                
                self.tblProductList.reloadData()
                
            }
        }
    }
    

    @IBAction func goNextAction(_ sender: Any) {
        
        let transactionVC = self.storyboard?.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
        transactionVC.username = lblUserName.text
        self.navigationController?.pushViewController(transactionVC, animated: true)
        
    }
    
    
    @IBAction func userAction(_ sender: Any) {
        
        inputUser()
    }
    

    func inputUser() {
  
        let alert = UIAlertController(title: "Input Email Address", message: "Enter a email", preferredStyle: .alert)

        alert.addTextField { (textField) in
            textField.text = ""
            textField.keyboardType = .emailAddress
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            if textField?.text?.count == 0 {
                textField?.text = "Walkin"
            }
            print("Text field: \(String(describing: textField?.text))")
            
            self.lblUserName.text = textField?.text
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func updateValues() {
        
        let subTotal = purchasedProducts.map( { $0.totalPrce } ).reduce(0, +)
        lblSubTotal.text = String(format: "%.2f", subTotal)
        lblDiscount.text = String(format: "%.2f", discount)
        lblGrantTotal.text = String(format: "%.2f", subTotal - discount)
    }
    
    // MARK: - TableView

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count:Int?
        
        if tableView == self.tblProductList {
            count = 2 * categories.count
        }
        
        if tableView == self.tblPurchasedList {
            count =  purchasedProducts.count
        }
        
        return count!
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height:CGFloat?
        
        if tableView == self.tblProductList {
            if indexPath.row % 2 == 0 {
                height = 40
            } else {
                
                let productListViewWidth: CGFloat = (self.view.bounds.width / 2 - 20)
                let itemCount = categories[indexPath.row / 2].products.count
                let availableWidth: CGFloat = CGFloat(itemCount) * productItemCellWidth
                let rowCount = Int(floor(availableWidth / productListViewWidth)) + 1
                height = CGFloat(rowCount) * productItemCellHeight + 10.0

            }
        }
        
        if tableView == self.tblPurchasedList {
            height = 80
        }
        
        return height!
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if tableView == self.tblProductList {
            
            if indexPath.row % 2 == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
                cell.textLabel!.text = categories[indexPath.row / 2].name
                
                return cell
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductCell
                
                return cell
                
            }
        }
        
        
        if tableView == self.tblPurchasedList {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PurchaseCell", for: indexPath) as! PurchaseCell
            
            
            let product = purchasedProducts[indexPath.row]
            
            
            cell.lblName.text = product.product?.product_name
            cell.lblDescription.text = product.product?.short_description
            cell.lblPrice.text = String(format: "%.2f", (product.product!.price as NSString).floatValue)
            cell.lblCount.text = "\(product.count)"
            cell.lblSubTtlPrice.text = String(format: "%.2f", product.totalPrce)
            
            return cell
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? ProductCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? ProductCell else { return }
        
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if tableView ==  self.tblPurchasedList {
            return true
        
        } else {
            return false
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            
            purchasedProducts.remove(at: indexPath.row)
            
            tblPurchasedList.reloadData()
            
            updateValues()
        }
    }

}


extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return categories[collectionView.tag / 2].products.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductItemCell", for: indexPath) as! ProductItemCell
        
        //cell.backgroundColor = model[collectionView.tag][indexPath.item]
        cell.imvItem.sd_setImage(with: URL(string: categories[collectionView.tag / 2].products[indexPath.row].thumbnail_url) , placeholderImage: UIImage(named: "images"))
        cell.lblName.text = categories[collectionView.tag / 2].products[indexPath.row].product_name

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        
        let product = categories[collectionView.tag / 2].products[indexPath.row]
        let purchaseProduct = PurchsedProductModel()
        
        var exist: Bool = false
        
        if purchasedProducts.count > 0 {
            
            for existProduct in purchasedProducts {
                
                if existProduct.product!.product_id == product.product_id {
                    
                    existProduct.count = existProduct.count + 1
                    existProduct.totalPrce = existProduct.totalPrce + (existProduct.product!.price as NSString).floatValue
                    exist = true
                    break
                }
            }
            
            if !exist {
                purchaseProduct.count = 1
                purchaseProduct.product = product
                purchaseProduct.totalPrce = Float(product.price)!
                
                purchasedProducts.append(purchaseProduct)
            }
            
        } else {
            purchaseProduct.count = 1
            purchaseProduct.product = product
            purchaseProduct.totalPrce = Float(product.price)!
            
            purchasedProducts.append(purchaseProduct)
        }
        
        tblPurchasedList.reloadData()
        
        updateValues()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: productItemCellWidth, height: productItemCellHeight)
    }
}

