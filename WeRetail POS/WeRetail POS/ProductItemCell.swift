//
//  ProductItemCell.swift
//  WeRetail POS
//
//  Created by Developer on 10/26/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class ProductItemCell: UICollectionViewCell {
    
    @IBOutlet weak var imvItem: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    
}
