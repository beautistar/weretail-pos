//
//  CategoryModel.swift
//  WeRetail POS
//
//  Created by Developer on 10/29/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryModel {

    var name: String = ""
    var products: [ProductModel] = []
    
    init?(json: JSON?) {
        guard let json = json else { return nil }
        name = json["category"].stringValue
        products = json["product"].arrayValue.compactMap { ProductModel(json: $0) }
    }

}
