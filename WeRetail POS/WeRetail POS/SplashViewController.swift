//
//  SplashViewController.swift
//  WeRetail POS
//
//  Created by Developer on 10/26/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.isHidden = true
        
        self.perform(#selector(gotoStart), with: nil, afterDelay: 4)
    }
    
    
    @objc func gotoStart() {
        
        let startVC = self.storyboard?.instantiateViewController(withIdentifier: "StartViewController") as! StartViewController
        self.navigationController?.pushViewController(startVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
