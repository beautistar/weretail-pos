//
//  TransactionViewController.swift
//  WeRetail POS
//
//  Created by Developer on 10/26/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import UIKit

class TransactionViewController: UIViewController {
    
    @IBOutlet weak var lblCustomer: UILabel!
    var username: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        lblCustomer.text = "Customer \n" + username!
        
    }
    

    @IBAction func confirmAction(_ sender: Any) {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
}
